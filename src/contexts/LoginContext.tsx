//// External imports
import axios from "axios";
import { createContext, useState, ReactNode, useContext } from "react";
import Router from "next/router";

//// Types
// Type for data in login process
type valuesData = {
  key?: string;
  user?: string;
  password?: string;
  server: string;
};

// Type for all exports of context
type LoginContextData = {
  GetLoginKey: (formValues: valuesData) => void;
  LogOut: () => void;
  setIsError: (value:boolean) => void;
  setIsKeyLogin: (value:boolean) => void;
  saved: boolean;
  isError: boolean;
  isKeyLogin: boolean;
};

//Type for context access in all code
type LoginContextProviderProps = {
  children: ReactNode;
};

// Creation of context
export const LoginContext = createContext({} as LoginContextData);

// Creation of consts and functions shared
export function LoginContextProvider({ children }: LoginContextProviderProps) {
  const [ saved, setSaved ] = useState(false);
  const [ isError, setIsError ] = useState(false);
  const [ isKeyLogin, setIsKeyLogin ] = useState(false);

  // Chave API Zabbix = d0da5a206a21bac1f5d5ea4a2f15fbc6beb46f152d1ec298c19bade56b1f76b7

  var CryptoJS = require("crypto-js");

  function GetLoginKey(formValues: valuesData) {

    if (isKeyLogin) {
      sessionStorage.setItem("zabbixKey", formValues.key);
      sessionStorage.setItem("zabbixServer", formValues.server);
      setSaved(true);
    }
    else {
      axios({
        url: formValues.server,
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        data: JSON.stringify({
          jsonrpc: "2.0",
          method: "user.login",
          params: {
            user: formValues.user,
            password: formValues.password,
          },
          id: 1,
          auth: null,
        }),
      })
      .then((response) => {
        if (response.data.error ) {
          setIsError(true)
        }
        else {
          sessionStorage.setItem("zabbixKey", response.data.result);
          sessionStorage.setItem("zabbixServer", formValues.server);
          setSaved(true);
          setIsError(false);
        }
      })
      .catch((error) => {
        setIsError(true)
      })
    }
  }

  function LogOut() {
    setSaved(false), sessionStorage.removeItem("zabbixKey");
    sessionStorage.removeItem("zabbixServer");
    Router.push("/");
  }

  return (
    <LoginContext.Provider
      value={{
        GetLoginKey,
        saved,
        LogOut,
        isError,
        setIsError,
        isKeyLogin,
        setIsKeyLogin
      }}
    >
      {children}
    </LoginContext.Provider>
  );
}

export const useLogin = () => {
  return useContext(LoginContext);
};
